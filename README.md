# Modul Web-Mining

Dieses Repository enthält Material, Code-Beispiele und Übungsaufgaben zum Modul [Web-Mining](https://elearning.fh-swf.de/course/view.php?id=7512) an der 
[Fachhochschule Südwestfalen](https://www.fh-swf.de).

Das Inhaltsverzeichnis befindet sich in [index.ipynb](index.ipynb).
